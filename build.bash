#!/bin/bash

set -ex

if [ "$BOARD" = "robin" ]
then
  echo "board: robin"
  export CROSS_COMPILE=arm-linux-gnueabihf-
  export ARCH=arm
  make zynq_robin_defconfig
  make -j 8
  mkdir -p iv_output
  cp -f arch/arm/dts/zynq-robin.dtb iv_output/devicetree.dtb
  cp -f u-boot.elf iv_output/u-boot.elf
elif [ "$BOARD" = "falconi" ]
then
  echo "board: falconig"
  export CROSS_COMPILE=aarch64-linux-gnu-
  export ARCH=arm64
  mkdir -p iv_output
  make xilinx_zynqmp_defconfig
  make -j 8
  cp -f arch/arm/dts/zynqmp-inno.dtb iv_output/devicetree.dtb
  cp -f u-boot iv_output/u-boot.elf
  
  echo "board: falcong-g5"
  make clean
  make xilinx_zynqmp_g5_defconfig
  make -j 8
  cp -f arch/arm/dts/zynqmp-inno-g5.dtb iv_output/devicetree_g5.dtb
  cp -f u-boot iv_output/u-boot_g5.elf

  echo "board: falconi-m1g"
  make clean
  make xilinx_zynqmp_m1g_defconfig
  make -j 8
  cp -f arch/arm/dts/zynqmp-inno-m1g.dtb iv_output/devicetree_m1g.dtb
  cp -f u-boot iv_output/u-boot_m1g.elf

  echo "board: falcon10k-sgmii"
  make clean
  make xilinx_zynqmp_sgmii_defconfig
  make -j 8
  cp -f arch/arm/dts/zynqmp-inno-sgmii.dtb iv_output/devicetree_sgmii.dtb
  cp -f u-boot iv_output/u-boot_sgmii.elf

else
  echo "BOARD is not valid: ${BOARD}"
  exit 1
fi
