// SPDX-License-Identifier: GPL-2.0+
/*
 * RealTek PHY drivers
 *
 * Copyright 2010-2011, 2015 Freescale Semiconductor, Inc.
 * author Andy Fleming
 * Copyright 2016 Karsten Merker <merker@debian.org>
 */
#include <common.h>
#include <linux/bitops.h>
#include <phy.h>

#define PHY_RTL8211x_FORCE_MASTER BIT(1)
#define PHY_RTL8211E_PINE64_GIGABIT_FIX BIT(2)

#define PHY_AUTONEGOTIATE_TIMEOUT 5000

/* RTL8211x 1000BASE-T Control Register */
#define MIIM_RTL8211x_CTRL1000T_MSCE BIT(12);
#define MIIM_RTL8211x_CTRL1000T_MASTER BIT(11);

/* RTL8211x PHY Status Register */
#define MIIM_RTL8211x_PHY_STATUS       0x11
#define MIIM_RTL8211x_PHYSTAT_SPEED    0xc000
#define MIIM_RTL8211x_PHYSTAT_GBIT     0x8000
#define MIIM_RTL8211x_PHYSTAT_100      0x4000
#define MIIM_RTL8211x_PHYSTAT_DUPLEX   0x2000
#define MIIM_RTL8211x_PHYSTAT_SPDDONE  0x0800
#define MIIM_RTL8211x_PHYSTAT_LINK     0x0400

/* RTL8211x PHY Interrupt Enable Register */
#define MIIM_RTL8211x_PHY_INER         0x12
#define MIIM_RTL8211x_PHY_INTR_ENA     0x9f01
#define MIIM_RTL8211x_PHY_INTR_DIS     0x0000

/* RTL8211x PHY Interrupt Status Register */
#define MIIM_RTL8211x_PHY_INSR         0x13

/* RTL8211F PHY Status Register */
#define MIIM_RTL8211F_PHY_STATUS       0x1a
#define MIIM_RTL8211F_AUTONEG_ENABLE   0x1000
#define MIIM_RTL8211F_PHYSTAT_SPEED    0x0030
#define MIIM_RTL8211F_PHYSTAT_GBIT     0x0020
#define MIIM_RTL8211F_PHYSTAT_100      0x0010
#define MIIM_RTL8211F_PHYSTAT_DUPLEX   0x0008
#define MIIM_RTL8211F_PHYSTAT_SPDDONE  0x0800
#define MIIM_RTL8211F_PHYSTAT_LINK     0x0004

#define MIIM_RTL8211E_CONFREG           0x1c
#define MIIM_RTL8211E_CONFREG_TXD		0x0002
#define MIIM_RTL8211E_CONFREG_RXD		0x0004
#define MIIM_RTL8211E_CONFREG_MAGIC		0xb400	/* Undocumented */

#define MIIM_RTL8211E_EXT_PAGE_SELECT  0x1e

#define MIIM_RTL8211F_PAGE_SELECT      0x1f
#define MIIM_RTL8211F_TX_DELAY		0x100
#define MIIM_RTL8211F_LCR		0x10


/* RTL9010A PHY*/
#define MIIM_RTL9010A_PAGE_SELECT      0x1f
#define MIIM_RTL9010A_DEFAULT_PAGE     0x0a42

/* RTL9010A PHY Status Register */
#define MIIM_RTL9010A_PHY_STATUS       0x1a
#define MIIM_RTL9010A_PHYSTAT_MM       0x0800
#define MIIM_RTL9010A_PHYSTAT_SPEED    0x0030
#define MIIM_RTL9010A_PHYSTAT_GBIT     0x0020
#define MIIM_RTL9010A_PHYSTAT_100      0x0010
#define MIIM_RTL9010A_PHYSTAT_DUPLEX   0x0008
#define MIIM_RTL9010A_PHYSTAT_LINK     0x0004

/* RTL9010A PHY Interrupt Enable Register*/
#define MIIM_RTL9010A_PHY_INER         0x12
#define MIIM_RTL9010A_PHY_INTR_DIS     0x0000

static int rtl8211b_probe(struct phy_device *phydev)
{
#ifdef CONFIG_RTL8211X_PHY_FORCE_MASTER
	phydev->flags |= PHY_RTL8211x_FORCE_MASTER;
#endif

	return 0;
}

static int rtl8211e_probe(struct phy_device *phydev)
{
#ifdef CONFIG_RTL8211E_PINE64_GIGABIT_FIX
	phydev->flags |= PHY_RTL8211E_PINE64_GIGABIT_FIX;
#endif

	return 0;
}

/* RealTek RTL8211x */
static int rtl8211x_config(struct phy_device *phydev)
{
	phy_write(phydev, MDIO_DEVAD_NONE, MII_BMCR, BMCR_RESET);

	/* mask interrupt at init; if the interrupt is
	 * needed indeed, it should be explicitly enabled
	 */
	phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211x_PHY_INER,
		  MIIM_RTL8211x_PHY_INTR_DIS);

	if (phydev->flags & PHY_RTL8211x_FORCE_MASTER) {
		unsigned int reg;

		reg = phy_read(phydev, MDIO_DEVAD_NONE, MII_CTRL1000);
		/* force manual master/slave configuration */
		reg |= MIIM_RTL8211x_CTRL1000T_MSCE;
		/* force master mode */
		reg |= MIIM_RTL8211x_CTRL1000T_MASTER;
		phy_write(phydev, MDIO_DEVAD_NONE, MII_CTRL1000, reg);
	}
	if (phydev->flags & PHY_RTL8211E_PINE64_GIGABIT_FIX) {
		unsigned int reg;

		phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211F_PAGE_SELECT,
			  7);
		phy_write(phydev, MDIO_DEVAD_NONE,
			  MIIM_RTL8211E_EXT_PAGE_SELECT, 0xa4);
		reg = phy_read(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211E_CONFREG);
		/* Ensure both internal delays are turned off */
		reg &= ~(MIIM_RTL8211E_CONFREG_TXD | MIIM_RTL8211E_CONFREG_RXD);
		/* Flip the magic undocumented bits */
		reg |= MIIM_RTL8211E_CONFREG_MAGIC;
		phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211E_CONFREG, reg);
		phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211F_PAGE_SELECT,
			  0);
	}
	/* read interrupt status just to clear it */
	phy_read(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211x_PHY_INER);

	genphy_config_aneg(phydev);

	return 0;
}

static int rtl8211f_config(struct phy_device *phydev)
{
	u16 reg;

	phy_write(phydev, MDIO_DEVAD_NONE, MII_BMCR, BMCR_RESET);

	phy_write(phydev, MDIO_DEVAD_NONE,
		  MIIM_RTL8211F_PAGE_SELECT, 0xd08);
	reg = phy_read(phydev, MDIO_DEVAD_NONE, 0x11);

	/* enable TX-delay for rgmii-id and rgmii-txid, otherwise disable it */
	if (phydev->interface == PHY_INTERFACE_MODE_RGMII_ID ||
	    phydev->interface == PHY_INTERFACE_MODE_RGMII_TXID)
		reg |= MIIM_RTL8211F_TX_DELAY;
	else
		reg &= ~MIIM_RTL8211F_TX_DELAY;

	phy_write(phydev, MDIO_DEVAD_NONE, 0x11, reg);
	/* restore to default page 0 */
	phy_write(phydev, MDIO_DEVAD_NONE,
		  MIIM_RTL8211F_PAGE_SELECT, 0x0);

	/* Set green LED for Link, yellow LED for Active */
	phy_write(phydev, MDIO_DEVAD_NONE,
		  MIIM_RTL8211F_PAGE_SELECT, 0xd04);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x10, 0x617f);
	phy_write(phydev, MDIO_DEVAD_NONE,
		  MIIM_RTL8211F_PAGE_SELECT, 0x0);

	genphy_config_aneg(phydev);

	return 0;
}

static int rtl8211x_parse_status(struct phy_device *phydev)
{
	unsigned int speed;
	unsigned int mii_reg;

	mii_reg = phy_read(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211x_PHY_STATUS);

	if (!(mii_reg & MIIM_RTL8211x_PHYSTAT_SPDDONE)) {
		int i = 0;

		/* in case of timeout ->link is cleared */
		phydev->link = 1;
		puts("Waiting for PHY realtime link");
		while (!(mii_reg & MIIM_RTL8211x_PHYSTAT_SPDDONE)) {
			/* Timeout reached ? */
			if (i > PHY_AUTONEGOTIATE_TIMEOUT) {
				puts(" TIMEOUT !\n");
				phydev->link = 0;
				break;
			}

			if ((i++ % 1000) == 0)
				putc('.');
			udelay(1000);	/* 1 ms */
			mii_reg = phy_read(phydev, MDIO_DEVAD_NONE,
					MIIM_RTL8211x_PHY_STATUS);
		}
		puts(" done\n");
		udelay(500000);	/* another 500 ms (results in faster booting) */
	} else {
		if (mii_reg & MIIM_RTL8211x_PHYSTAT_LINK)
			phydev->link = 1;
		else
			phydev->link = 0;
	}

	if (mii_reg & MIIM_RTL8211x_PHYSTAT_DUPLEX)
		phydev->duplex = DUPLEX_FULL;
	else
		phydev->duplex = DUPLEX_HALF;

	speed = (mii_reg & MIIM_RTL8211x_PHYSTAT_SPEED);

	switch (speed) {
	case MIIM_RTL8211x_PHYSTAT_GBIT:
		phydev->speed = SPEED_1000;
		break;
	case MIIM_RTL8211x_PHYSTAT_100:
		phydev->speed = SPEED_100;
		break;
	default:
		phydev->speed = SPEED_10;
	}

	return 0;
}

static int rtl8211f_parse_status(struct phy_device *phydev)
{
	unsigned int speed;
	unsigned int mii_reg;
	int i = 0;

	phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211F_PAGE_SELECT, 0xa43);
	mii_reg = phy_read(phydev, MDIO_DEVAD_NONE, MIIM_RTL8211F_PHY_STATUS);

	phydev->link = 1;
	while (!(mii_reg & MIIM_RTL8211F_PHYSTAT_LINK)) {
		if (i > PHY_AUTONEGOTIATE_TIMEOUT) {
			puts(" TIMEOUT !\n");
			phydev->link = 0;
			break;
		}

		if ((i++ % 1000) == 0)
			putc('.');
		udelay(1000);
		mii_reg = phy_read(phydev, MDIO_DEVAD_NONE,
				   MIIM_RTL8211F_PHY_STATUS);
	}

	if (mii_reg & MIIM_RTL8211F_PHYSTAT_DUPLEX)
		phydev->duplex = DUPLEX_FULL;
	else
		phydev->duplex = DUPLEX_HALF;

	speed = (mii_reg & MIIM_RTL8211F_PHYSTAT_SPEED);

	switch (speed) {
	case MIIM_RTL8211F_PHYSTAT_GBIT:
		phydev->speed = SPEED_1000;
		break;
	case MIIM_RTL8211F_PHYSTAT_100:
		phydev->speed = SPEED_100;
		break;
	default:
		phydev->speed = SPEED_10;
	}

	return 0;
}

static int rtl8211x_startup(struct phy_device *phydev)
{
	int ret;

	/* Read the Status (2x to make sure link is right) */
	ret = genphy_update_link(phydev);
	if (ret)
		return ret;

	return rtl8211x_parse_status(phydev);
}

static int rtl8211e_startup(struct phy_device *phydev)
{
	int ret;

	ret = genphy_update_link(phydev);
	if (ret)
		return ret;

	return genphy_parse_link(phydev);
}

static int rtl8211f_startup(struct phy_device *phydev)
{
	int ret;

	/* Read the Status (2x to make sure link is right) */
	ret = genphy_update_link(phydev);
	if (ret)
		return ret;
	/* Read the Status (2x to make sure link is right) */

	return rtl8211f_parse_status(phydev);
}

static inline void rtl9010ax_spr_write(struct phy_device *phydev,
		int devad, int addr, u16 val)
{
	phy_write(phydev, devad, 27, addr);
	phy_write(phydev, devad, 28, val);
}

static inline int rtl9010ax_spr_read(struct phy_device *phydev,
		int devad, int addr)
{
	phy_write(phydev, devad, 27, addr);
	return phy_read(phydev, devad, 28);
}

static inline void rtl9010ax_page_write(struct phy_device *phydev,
		int page, int addr, u16 val)
{
	phy_write(phydev, MDIO_DEVAD_NONE, 31, page);
	phy_write(phydev, MDIO_DEVAD_NONE, addr, val);
	/* restore to default page*/
	phy_write(phydev, MDIO_DEVAD_NONE, 31, MIIM_RTL9010A_DEFAULT_PAGE);
}

static inline int rtl9010ax_page_read(struct phy_device *phydev,
		int page, int addr)
{
	phy_write(phydev, MDIO_DEVAD_NONE, 31, page);
	return phy_read(phydev, MDIO_DEVAD_NONE, addr);
}

static void rtl9010ax_soft_reset(struct phy_device *phydev)
{
	int count;

	/* software reset*/
	phy_write(phydev, MDIO_DEVAD_NONE, MII_BMCR, BMCR_RESET);
	/* Check soft-reset complete*/
	for (count = 50; count > 0; count--) {
		if (phy_read(phydev, MDIO_DEVAD_NONE, MII_BMCR) == 0x0140) {
			break;
		}
	}
}

#define rtl9010ax_write_spr(phydev, spr, val) \
              rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, spr, val)
#define rtl9010ax_read_spr(phydev, spr) \
              rtl9010ax_spr_read(phydev, MDIO_DEVAD_NONE, spr)

// Refer to Errata_List_v0.4.pdf, Erratum#1
static int rtl9010ax_initial_config(struct phy_device *phydev)
{
	int ret;
	u32 timer;

	// PHY Parameter Start
	rtl9010ax_page_write(phydev, 0x0bc4, 0x15, 0x16fe);

	rtl9010ax_write_spr(phydev, 0xb820, 0x0010);
	rtl9010ax_write_spr(phydev, 0xb830, 0x8000);
	timer = 3;
	for(;;) {
		ret = rtl9010ax_read_spr(phydev, 0xb800);
		if (ret < 0)
			return ret;
		if (ret & 0x0040)
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_write_spr(phydev, 0x8020, 0x9100);
	rtl9010ax_write_spr(phydev, 0xb82e, 0x0001);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0290);
	rtl9010ax_write_spr(phydev, 0xa012, 0x0000);
	rtl9010ax_write_spr(phydev, 0xa014, 0xd700);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x880f);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x262d);
	rtl9010ax_write_spr(phydev, 0xa01a, 0x0000);
	rtl9010ax_write_spr(phydev, 0xa000, 0x162c);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0210);
	rtl9010ax_write_spr(phydev, 0xb82e, 0x0000);
	rtl9010ax_write_spr(phydev, 0x8020, 0x0000);
	rtl9010ax_write_spr(phydev, 0xb820, 0x0000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xb800);
		if (ret < 0)
			return ret;
		if (!(ret & 0x0040))
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_soft_reset(phydev);

	return 0;
}

// Refer to Errata_List_v0.4.pdf, Erratum#4
static int rtl9010ax_initial_sgmii(struct phy_device *phydev)
{
	int ret;
	u32 timer;

	rtl9010ax_write_spr(phydev, 0xB820, 0x0010);
	rtl9010ax_write_spr(phydev, 0xB830, 0x8000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xB800);
		if (ret < 0)
			return ret;
		if (ret & 0x0040)
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_write_spr(phydev, 0x8020, 0x9100);
	rtl9010ax_write_spr(phydev, 0xB82E, 0x0001);
	rtl9010ax_write_spr(phydev, 0x8614, 0xAF86);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x20AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x862A);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAF86);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x2AAF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x862A);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEE82);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xF600);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0286);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x2AAF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x096B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xF8F9);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF59);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xF9FA);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xFBBF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8768);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x023C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5F00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAD28);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x1AAE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x03AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x86E4);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE187);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6BD0);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x001B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x019E);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x13D0);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x011B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x019E);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x34D0);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x021B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x019E);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE8EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x00AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8748);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5002);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x3C5F);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAC28);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x03AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8748);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5302);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x43BB);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xC3EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0102);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x42E7);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF47);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE487);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6CE5);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876D);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x48BF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8756);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x023C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5FAC);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x281A);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE087);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6CE1);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876D);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF64);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE087);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6EE1);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876F);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF74);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x02AC);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5035);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x00AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8748);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5902);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x43BB);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xC3EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x02BF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8765);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBBBF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x875C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBBBF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8762);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBBEE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8774);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0002);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x42E7);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF47);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE487);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x70E5);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8771);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x48EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x00EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8775);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x01AF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8748);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE087);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x70E1);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8771);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF64);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE087);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x72E1);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8773);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF74);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0243);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x02AD);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x504E);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5C02);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x3C5F);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x3C00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x009F);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0DBF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x875F);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x023C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5F3C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x9F02);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xAE27);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6502);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x43BB);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x5C02);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x43BB);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xBF87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x6202);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x43BB);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE187);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x7411);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE587);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x7439);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x03AA);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x19EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x876B);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x00EE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8776);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x01AE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0F02);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x42E7);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEF47);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xE487);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x70E5);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x8771);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEE87);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x7400);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xFFFE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xFDEF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x95FD);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xFC04);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xEECF);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0411);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xCC00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x00CC);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x7A00);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xD04A);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xF0CC);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x40F8);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xCC42);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xF0CC);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x4420);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0xCC70);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x11CE);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x003C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x003C);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);
	phy_write(phydev, MDIO_DEVAD_NONE, 0x1c, 0x0000);

	rtl9010ax_write_spr(phydev, 0xB818, 0x0967);
	rtl9010ax_write_spr(phydev, 0xB81A, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB81C, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB81E, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB832, 0x0001);
	rtl9010ax_write_spr(phydev, 0xB82E, 0x0000);
	rtl9010ax_write_spr(phydev, 0x8020, 0x0000);
	rtl9010ax_write_spr(phydev, 0xB820, 0x0000);
	timer = 3;
	for (;;) {
		ret = rtl9010ax_read_spr(phydev, 0xB800);
		if (ret < 0)
			return ret;
		if (!(ret & 0x0040))
			break;
		if (--timer == 0)
			return -ETIMEDOUT;
		mdelay(1);
	}

	rtl9010ax_soft_reset(phydev);

	return 0;
}

static int rtl9010ax_initial(struct phy_device *phydev)
{
	int ret;

	ret = rtl9010ax_initial_config(phydev);
	if (ret != 0) {
		printf("rtl9010ax init config fail\n");
		return ret;
	}

	if (phydev->interface == PHY_INTERFACE_MODE_SGMII) {
		printf("rtl9010ax sgmii init\n");
		ret = rtl9010ax_initial_sgmii(phydev);
	}

	return ret;
}

 // Refer to Errata_List_v0.4.pdf, Erratum#3
 typedef enum {
	Weak_RGMII_3V3    = 1,
	Typical_RGMII_3V3 = 2,
	Strong_RGMII_3V3  = 3,
	Weak_RGMII_2V5    = 4,
	Typical_RGMII_2V5 = 5,
	Strong_RGMII_2V5  = 6,
	Weak_RGMII_1V8    = 7,
	Typical_RGMII_1V8 = 8,
	Strong_RGMII_1V8  = 9
} RGMII_Voltage;

static int rtl9010ax_rgmii_driving_strength(struct phy_device *phydev, u16 RGMII_Voltage)
{
	switch (RGMII_Voltage){
	case Weak_RGMII_3V3:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0605);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0505);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0600);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0500);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xa3a3);
		break;

	case Typical_RGMII_3V3:
		rtl9010ax_write_spr(phydev, 0xd414, 0x1615);
		rtl9010ax_write_spr(phydev, 0xd416, 0x1515);
		rtl9010ax_write_spr(phydev, 0xd418, 0x1600);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x1500);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x80a4);
		break;

	case Weak_RGMII_2V5:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x2525);
		break;

	case Typical_RGMII_2V5:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xffff);
		break;

	case Weak_RGMII_1V8:
		rtl9010ax_write_spr(phydev, 0xd414, 0x0201);
		rtl9010ax_write_spr(phydev, 0xd416, 0x0101);
		rtl9010ax_write_spr(phydev, 0xd418, 0x0200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x0100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0xc8c8);
		break;

	case Typical_RGMII_1V8:
		rtl9010ax_write_spr(phydev, 0xd414, 0x1211);
		rtl9010ax_write_spr(phydev, 0xd416, 0x1111);
		rtl9010ax_write_spr(phydev, 0xd418, 0x1200);
		rtl9010ax_write_spr(phydev, 0xd41a, 0x1100);
		rtl9010ax_write_spr(phydev, 0xd42e, 0x7e7e);
		break;

	default:
		break;
	}
	return 0;
}

#define rtl9010ax_delay_effect(phydev) \
	rtl9010ax_soft_reset(phydev)
static void rtl9010ax_1000m_txc_delay(struct phy_device *phydev,
		u16 delay, u16 speed)
{
	u16 rgtr2;
	u16 rgtr3;

	rgtr2 = rtl9010ax_spr_read(phydev, MDIO_DEVAD_NONE, 0xd084);

	rgtr3 = rtl9010ax_spr_read(phydev, MDIO_DEVAD_NONE, 0xd082);

	/* use the following step to change the level of 1000M_RGMII_TX_Timing*/

	/* step1: Set 1000M_TXC_nodelay_en(bit7) to 0*/
	rgtr2 &= ~0x0080;
	rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step2: Set 1000M_RGMII_TXC_timing_delay_adjust_speed(bit[1:0])
	 * to 2'b11
	 */
	rgtr3 |= 0x0003;
	rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd082, rgtr3);

	/* step3: Change 1000M_RGMII_TXC_Timing(bit[15:14]) to desired level*/
	rgtr2 &= ~0xc000;
	rgtr2 |= ((delay << 14) & 0xc000);
	rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step4: Set 1000M_RGMII_TXC_timing_delay_en(bit[2:0]) to 3'b111*/
	rgtr2 |= 0x0007;
	rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
	rtl9010ax_delay_effect(phydev);

	/* step5: if the user want to change the level, set
	 * 1000M_RGMII_TXC_timing_delay_en(bit[2:0]) to 3'b000
	 */
	if (speed != 0x0003) {
		rgtr2 &= ~0x0007;
		rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);

		/* step6: change 1000M_RGMII_TXC_timing_delay_adjust_speed(bit[1:0])
		 * to desired level
		 */
		rgtr3 &= ~0x0003;
		rgtr3 |= (speed & 0x0003);
		rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd082, rgtr3);
		rtl9010ax_delay_effect(phydev);


		/* step7: set 1000M_RGMII_TXC_timing_delay_en(bit[2:0])
		 * to 3'b111 again
		 */
		rgtr2 |= 0x0007;
		rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);
	}

	if (delay == 0x0003) { //no delay
		rgtr2 |= 0x0080;
		rgtr2 &= ~0x0007;
		rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
		rtl9010ax_delay_effect(phydev);
	} else {
		/* TODO: Set 1000M_TXC_nodelay_en(bit7) to 1
		 * rgtr2 |= 0x0080;
		 * rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd084, rgtr2);
		 */
	}
}

static void rtl9010ax_1000m_rxc_delay(struct phy_device *phydev, u16 delay)
{
	u16 rgtr1;

	rgtr1 = rtl9010ax_spr_read(phydev, MDIO_DEVAD_NONE, 0xd04a);

	if (delay) {
		rgtr1 |= 0x0004;
	} else {
		rgtr1 &= ~0x0004;
	}
	rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xd04a, rgtr1);
}

static int rtl9010ax_config(struct phy_device *phydev)
{
	unsigned int mii_reg;
	int timer;

	/* check phy is ready*/
	for (timer = 0; timer < 6; timer++) {
		mii_reg = phy_read(phydev, MDIO_DEVAD_NONE, 0x10);
		if ((mii_reg & 0x0003) == 0x0003) {
			break;
		}
		mdelay(1);
	}

	// load Errata
	rtl9010ax_initial(phydev);

	/* Power Select */
	rtl9010ax_page_write(phydev, 0x0a4c, 0x12, 0x10FF);

	/* for speed1000M enable TX-delay for rgmii-id and rgmii-txid,
	 * otherwise disable it
	 */
	if (phydev->interface == PHY_INTERFACE_MODE_RGMII_ID ||
			phydev->interface == PHY_INTERFACE_MODE_RGMII_TXID) {
		rtl9010ax_1000m_txc_delay(phydev, 0x02, 0x03);
		rtl9010ax_1000m_rxc_delay(phydev, 0x01);
	} else {
		rtl9010ax_1000m_txc_delay(phydev, 0x02, 0x03);
		rtl9010ax_1000m_rxc_delay(phydev, 0x01);
	}

	/* adjust the driving strength for rgmii*/
	if (phydev->interface == PHY_INTERFACE_MODE_RGMII || 
		phydev->interface == PHY_INTERFACE_MODE_RGMII_ID ||
		phydev->interface == PHY_INTERFACE_MODE_RGMII_RXID || 
		phydev->interface == PHY_INTERFACE_MODE_RGMII_TXID) {
		rtl9010ax_rgmii_driving_strength(phydev, Weak_RGMII_3V3);
	}

	/* adjust tx amplitude for sgmii
	 * 0x0048 --- 700mv, 0x005a --- 580mv, 0x006c --- 450mv
	 */
	if (phydev->interface == PHY_INTERFACE_MODE_SGMII) {
		rtl9010ax_page_write(phydev, 0x0cd1, 0x16, 0x0048);
	}

	rtl9010ax_soft_reset(phydev);

	/* set speed to 1000Mbps */
	phy_write(phydev, MDIO_DEVAD_NONE, MII_BMCR,
					BMCR_FULLDPLX | BMCR_SPEED1000);

	/* mask interrupt at init; if the interrupt is
	 * needed indeed, it should be explicitly enabled
	*/
	phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL9010A_PHY_INER,
				MIIM_RTL9010A_PHY_INTR_DIS);

	/* config led */
	// rtl9010ax_spr_write(phydev, MDIO_DEVAD_NONE, 0xD040, 0xC627);

	/* restore to default page*/
	phy_write(phydev, MDIO_DEVAD_NONE, MIIM_RTL9010A_PAGE_SELECT,
						 MIIM_RTL9010A_DEFAULT_PAGE);

	phydev->pause = 0;
	phydev->asym_pause = 0;
	phydev->autoneg = AUTONEG_DISABLE;

	return 0;
}

int rtl9010ax_update_link(struct phy_device *phydev)
{
	unsigned int speed;
	unsigned int mii_reg;

	mii_reg = phy_read(phydev, MDIO_DEVAD_NONE, MIIM_RTL9010A_PHY_STATUS);
	printf("rtl9010ax status = 0x%x\n", mii_reg);

	if (mii_reg & MIIM_RTL9010A_PHYSTAT_LINK) {
		phydev->link = 1;
	} else {
		phydev->link = 0;
	}

	if (mii_reg & MIIM_RTL9010A_PHYSTAT_DUPLEX) {
		phydev->duplex = DUPLEX_FULL;
	} else {
		phydev->duplex = DUPLEX_HALF;
	}

	speed = (mii_reg & MIIM_RTL9010A_PHYSTAT_SPEED);

	switch (speed) {
	case MIIM_RTL9010A_PHYSTAT_GBIT:
		phydev->speed = SPEED_1000;
		break;
	case MIIM_RTL9010A_PHYSTAT_100:
		phydev->speed = SPEED_100;
		break;
	default:
		phydev->speed = SPEED_10;
		break;
	}

	printf("rtl9010ax link = %d, duplex = %d, speed %dM\n",
			phydev->link, phydev->duplex, phydev->speed);

	return 0;
}

static int rtl9010ax_startup(struct phy_device *phydev)
{
	/* Read the Status (2x to make sure link is right) */
	return rtl9010ax_update_link(phydev);
}


/* Support for RTL8211B PHY */
static struct phy_driver RTL8211B_driver = {
	.name = "RealTek RTL8211B",
	.uid = 0x1cc912,
	.mask = 0xffffff,
	.features = PHY_GBIT_FEATURES,
	.probe = &rtl8211b_probe,
	.config = &rtl8211x_config,
	.startup = &rtl8211x_startup,
	.shutdown = &genphy_shutdown,
};

/* Support for RTL8211E-VB-CG, RTL8211E-VL-CG and RTL8211EG-VB-CG PHYs */
static struct phy_driver RTL8211E_driver = {
	.name = "RealTek RTL8211E",
	.uid = 0x1cc915,
	.mask = 0xffffff,
	.features = PHY_GBIT_FEATURES,
	.probe = &rtl8211e_probe,
	.config = &rtl8211x_config,
	.startup = &rtl8211e_startup,
	.shutdown = &genphy_shutdown,
};

/* Support for RTL8211DN PHY */
static struct phy_driver RTL8211DN_driver = {
	.name = "RealTek RTL8211DN",
	.uid = 0x1cc914,
	.mask = 0xffffff,
	.features = PHY_GBIT_FEATURES,
	.config = &rtl8211x_config,
	.startup = &rtl8211x_startup,
	.shutdown = &genphy_shutdown,
};

/* Support for RTL8211F PHY */
static struct phy_driver RTL8211F_driver = {
	.name = "RealTek RTL8211F",
	.uid = 0x1cc916,
	.mask = 0xffffff,
	.features = PHY_GBIT_FEATURES,
	.config = &rtl8211f_config,
	.startup = &rtl8211f_startup,
	.shutdown = &genphy_shutdown,
};

/* Support for RTL9010AA, RTL9010ARG, RTL9010AR, RTL9010AS PHYs*/
static struct phy_driver RTL9010Ax_driver = {
	.name = "RealTek RTL9010AX",
	.uid = 0x1ccb30,
	.mask = 0xffffff,
	.features = PHY_GBIT_FEATURES,
	.config = &rtl9010ax_config,
	.startup = &rtl9010ax_startup,
	.shutdown = &genphy_shutdown,
};

int phy_realtek_init(void)
{
	phy_register(&RTL8211B_driver);
	phy_register(&RTL8211E_driver);
	phy_register(&RTL8211F_driver);
	phy_register(&RTL8211DN_driver);
	phy_register(&RTL9010Ax_driver);

	return 0;
}
