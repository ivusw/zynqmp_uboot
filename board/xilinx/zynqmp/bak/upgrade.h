///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2021  Innovusion, Inc.
// All Rights Reserved.
//
//    upgrade.h
//
///////////////////////////////////////////////////////////////////////////////

#ifndef LIDAR_APP_UPGRADE_H_
#define LIDAR_APP_UPGRADE_H_

// for : fsbl / uboot / upgrade / preconfig

#ifdef __cplusplus
#include <string>
#include <vector>
#endif

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

// #define UPGRADE_DEBUG   printf
#define UPGRADE_DEBUG(...)

#ifndef atoi
#define atoi(x) simple_strtoul(x, NULL, 0);
#endif

//**********************************************************************
//
//                          MTD definitions
//
//**********************************************************************

/*
MTD0:		0x0000_0000	1M  			Golden fsbl, 读TAG1/TAG2,看哪个有效
MTD1:		0x0010_0000	4M+512K		jffs2 参数区
MTD2:		0x0058_0000	128K			Tag1Confirm
MTD3:		0x005A_0000	128K			Tag1Info
MTD4:		0x005C_0000	128K			Tag2Confirm
MTD5:		0x005E_0000	128K			Tag2Info

MTD6:		0x0060_0000	10M+896K  A:boot  <fsbl + pmufw + ATF + bit stream + uboot>
MTD7:		0x010E_0000	128K			A:dts  (devicetree)
MTD8:		0x0110_0000	10M			  A:linux
MTD9:		0x01B0_0000	18M			  A:rootfs
MTD10:	0x02D0_0000	6M				A:app/fw	(firmware)
MTD11:	0x0330_0000	10M			  A:app/pcs	(pointcloud_server)
MTD12:	0x03D0_0000	6M				A:app/python

MTD13:	0x0430_0000	10M+896K  B:boot
MTD14:	0x04DE_0000	128K			B:dts
MTD15:	0x04E0_0000	10M			  B:linux
MTD16:	0x0580_0000	18M			  B:rootfs
MTD17:	0x06A0_0000	6M				B:app/fw
MTD18:	0x0700_0000	10M			  B:app/pcs
MTD19:	0x07A0_0000	6M				B:app/python
*/

#define TAG1_CONFIRM_OFFSET	0x00580000
#define TAG1_INFO_OFFSET  	0x005A0000
#define TAG2_CONFIRM_OFFSET	0x005C0000
#define TAG2_INFO_OFFSET  	0x005E0000

#define kAllPartitionNum  20    // mtd0 -- mtd19
const unsigned int parts_offset[kAllPartitionNum] = {
	0x00000000,     		          // Golden fsbl
	0x00100000,      			        // jffs2 参数区
	TAG1_CONFIRM_OFFSET, 			    // TAG1
  TAG1_INFO_OFFSET,
	TAG2_CONFIRM_OFFSET,			    // TAG2
  TAG2_INFO_OFFSET,

  0x00600000,                   // A:boot
  0x010E0000,                   // A:dts
  0x01100000,                   // A:linux
  0x01B00000,                   // A:rootfs
  0x02D00000,                   // A:app/fw
  0x03300000,                   // A:app/pcs
  0x03D00000,                   // A:app/python

  0x04300000,                   // B:boot
  0x04DE0000,                   // B:dts
  0x04E00000,                   // B:linux
  0x05800000,                   // B:rootfs
  0x06A00000,                   // B:app/fw
  0x07000000,                   // B:app/pcs
  0x07A00000                    // B:app/python
};

#define kPartitionNum   7    // partition A/B number

enum Partition {
  PartBoot = 0,
  PartDts,
  PartLinux,
  PartRootfs,
  PartAppFirmware,
  PartAppPointcloud,
  PartAppPython
};

const char *partition_name[kPartitionNum] = {
  "boot",
  "dts",
  "linux",
  "rootfs",
  "app_firmware",
  "app_pointcloud",
  "app_python"
};

const int kJffs2MtdNum        = 1;
const int kTag1ConfirmMtdNum  = 2;
const int kTag1InfoMtdNum     = 3;
const int kTag2ConfirmMtdNum  = 4;
const int kTag2InfoMtdNum     = 5;

const unsigned int part_a_mtd_num[kPartitionNum] = {
  6,  7,  8,  9, 10, 11, 12
};

const unsigned int part_b_mtd_num[kPartitionNum] = {
  13, 14, 15, 16, 17, 18, 19
};

//**********************************************************************
//
//                     TagConfirm partition definitions
//
//**********************************************************************

// ex:  InnoMultiBoot Confirm ID=120
const char *confirm = "InnoMultiBoot Confirm ID=";
#define kMaxTagConfirmSize   64


//**********************************************************************
//
//                     TagInfo partition definitions
//
//**********************************************************************

#define mask_type             unsigned int

#define kMaxTagInfoSize       (4 * 1024)

#define kMd5Len     33
struct partition_info {
	unsigned int 	mtd_num;
	unsigned int 	offset;
	unsigned int 	used_len;         // actual length, not mtd length
	char          md5sum[kMd5Len];

  // runtime variable
  int need_upgrade;
};

#ifdef __cplusplus
bool operator==(const partition_info &t1, const partition_info &t2) {
  if (t1.mtd_num == t2.mtd_num &&
      t1.offset == t2.offset &&
      t1.used_len == t2.used_len &&
      strncmp(t1.md5sum, t2.md5sum, kMd5Len) == 0) {
    return true;
  } else {
    return false;
  }
}
#endif

#define MAGIC_LENGTH  16
const char *magic = "InnoMultiBoot";

struct tag_info {
  char		       magic[MAGIC_LENGTH];
  unsigned int   tag_num;    // tag 1/2
  unsigned int 	 id;
	struct partition_info pi[kPartitionNum];
#ifdef __cplusplus
  // fsbl/uboot will not use fw_version, then std::string is reasonable
  std::vector<std::string> fw_version;
  tag_info & operator = (const tag_info &src) {
    memcpy(magic, src.magic, sizeof(magic));
    tag_num = src.tag_num;
    id = src.id;
    memcpy(pi, src.pi, sizeof(pi));
    fw_version = src.fw_version;
    return *this;
  }
#endif  

  // runtime variables
  mask_type mask;
  int target_tag_confirm_mtd_num;
  int target_tag_info_mtd_num;
};

#ifdef __cplusplus
bool operator == (const tag_info &t1, const tag_info &t2) {
  if (strcmp(t1.magic, t2.magic) ||
      t1.tag_num != t2.tag_num ||
      t1.id != t2.id ||
      t1.fw_version != t2.fw_version) {
    return false;
  }
  for (int i = 0; i < kPartitionNum; ++i) {
    if (!(t1.pi[i] == t2.pi[i])) {
      return false;
    }
  }
  return true;
}
#endif

//**********************************************************************
//
//                       Analyze current tag
//
//**********************************************************************

const mask_type mask_ok = 0xffffffff;

// high bit number has priority
#define mask_magic        (1 << 31)
#define mask_tag_num      (1 << 30)
#define mask_id           (1 << 29)
#define mask_mtd_num(x)   (1 << (1 + (kPartitionNum - 1 - (x)) * 4 + 3))
#define mask_offset(x)    (1 << (1 + (kPartitionNum - 1 - (x)) * 4 + 2))
#define mask_used_len(x)  (1 << (1 + (kPartitionNum - 1 - (x)) * 4 + 1))
#define mask_md5(x)       (1 << (1 + (kPartitionNum - 1 - (x)) * 4 + 0))
#define mask_fw_version   (1 << 0)

#define MAX_KEY_LEN   32
#define MAX_VAL_LEN   256
#define MAX_LINE_LEN  (MAX_KEY_LEN + MAX_VAL_LEN + 2)   // 2: '=' '\n'

struct tag_para {
  char   *buf;
  ssize_t len;
  char    key[MAX_KEY_LEN];
  char    val[MAX_VAL_LEN];
};

int analyse_line(struct tag_para *para) {
  char *delimeter;
  char *lf;
  int  len;
  char *end = para->buf + para->len;

  if (!para->buf || para->len <= 0) {
    return -1;
  }

  delimeter = (char *)memchr(para->buf, '=', MAX_KEY_LEN + 1);
  if (!delimeter || delimeter >= end) {
    return -1;
  }

  lf = (char *)memchr(para->buf, '\n', MAX_LINE_LEN);
  if (!lf || lf >= end) {
    return -1;
  }

  len = lf - para->buf + 1;

  *lf = '\0';
  strncpy(para->val, delimeter + 1, MAX_VAL_LEN);
  para->val[MAX_VAL_LEN - 1] = '\0';

  *delimeter = '\0';
  strncpy(para->key, para->buf, MAX_KEY_LEN);
  para->key[MAX_KEY_LEN - 1] = '\0';

  para->buf += len;
  para->len -= len;
  return 0;
}

int get_index(const char *part_name) {
  for (int i = 0; i < kPartitionNum; ++i) {
    if (strcmp(part_name, partition_name[i]) == 0) {
      return i;
    }
  }
  return -1;
}

/**
 * @brief anaylyze MTD content, save to tag
 * @param buf read content of TagInfo partition
 * @param len buffer size
 * @return 0: success -1: fail
           tag->mask == mask_ok, otherwise missed some parameters
 */
int analyse_tag(char *buf, ssize_t len, struct tag_info *tag) {
  int part;
  struct tag_para para;
  para.buf = buf;
  para.len = len;

  if (!buf || len <= 0 || !tag) {
    return -1;
  }

  memset(tag, sizeof(*tag), 0);

  while (analyse_line(&para) == 0) {
    if (strcmp(para.key, "magic") == 0) {
      strncpy(tag->magic, para.val, MAGIC_LENGTH - 1);
      tag->magic[MAGIC_LENGTH - 1] = '\0';
      tag->mask |= mask_magic;
    } else if (strcmp(para.key, "tag_num") == 0) {
      tag->tag_num = atoi(para.val);
      if (tag->tag_num ==1 || tag->tag_num == 2) {
        tag->mask |= mask_tag_num;
      }
    } else if (strcmp(para.key, "id") == 0) {
      tag->id = atoi(para.val);
      tag->mask |= mask_id;
    } else if (strncmp(para.key, "mtd_num_", strlen("mtd_num_")) == 0) {
      part = get_index(&para.key[strlen("mtd_num_")]);
      if (part >= 0 && part < kPartitionNum) {
        tag->pi[part].mtd_num = atoi(para.val);
        tag->mask |= mask_mtd_num(part);
      }
    } else if (strncmp(para.key, "offset_", strlen("offset_")) == 0) {
      part = get_index(&para.key[strlen("offset_")]);
      if (part >= 0 && part < kPartitionNum) {
        tag->pi[part].offset = strtoul(para.val, NULL, 16);
        tag->mask |= mask_offset(part);
      }  
    } else if (strncmp(para.key, "used_len_", strlen("used_len_")) == 0) {
      part = get_index(&para.key[strlen("used_len_")]);
      if (part >= 0 && part < kPartitionNum) {
        tag->pi[part].used_len = strtoul(para.val, NULL, 16);
        tag->mask |= mask_used_len(part);
      }  
    } else if (strncmp(para.key, "md5sum_", strlen("md5sum_")) == 0) {
      part = get_index(&para.key[strlen("md5sum_")]);
      if (part >= 0 && part < kPartitionNum) {
        strncpy(tag->pi[part].md5sum, para.val, kMd5Len);
        tag->pi[part].md5sum[kMd5Len -1] = '\0';
        tag->mask |= mask_md5(part);
      }  
    } else if (strcmp(para.key, "fw_version") == 0) {
#ifdef __cplusplus
      tag->fw_version.push_back(para.val);
#endif
      tag->mask |= mask_fw_version;
    } else if (strcmp(para.key, "finish") == 0) {
      break;
    } else {
      // maybe we need to add new features, but fsbl/uboot do not need them
      // here fsbl/uboot do not need to upgrade themselves
      // but application should do with them
    }
  }

  return 0;
}

// read TagConfirm partition to decide which TagInfo is current
// @return: 
//          1  current tag1
//          2  current tag2
//         -1  error
int get_current_tag(const char *tag_1_confirm,
                    const char *tag_2_confirm) {
  int tag1_flag = 0;
  int tag1_id;
  int tag2_flag = 0;
  int tag2_id;

  if (!tag_1_confirm || !tag_1_confirm) {
    return -1;
  }

  if (memcmp(tag_1_confirm, confirm, strlen(confirm)) == 0) {
    tag1_flag = 1;
    tag1_id = atoi(tag_1_confirm + strlen(confirm));
    UPGRADE_DEBUG("tag confirm 1 id = %d\n", tag1_id);
  }

  if (memcmp(tag_2_confirm, confirm, strlen(confirm)) == 0) {
    tag2_flag = 1;
    tag2_id = atoi(tag_2_confirm + strlen(confirm));
    UPGRADE_DEBUG("tag confirm 2 id = %d\n", tag2_id);
  }

  if (tag1_flag != 1 && tag2_flag != 1) {
    return -1;
  }

  if (tag1_flag == 1 && tag2_flag == 1) {
    // find which id is bigger
    if (tag1_id >= tag2_id) {
      // tag1 valid, clear tag2
      tag2_flag = 0;
    } else {
      // tag2 valid, clear tag1
      tag1_flag = 0;
    }
  }

  if (tag1_flag) {
    UPGRADE_DEBUG("current tag confirm 1\n");
    return 1;
  } else if (tag2_flag) {
    UPGRADE_DEBUG("current tag confirm 2\n");
    return 2;
  }

  return -1;
}

// if can not get current tag from TagConfirm
// we still need to go to uboot
int get_current_tag_info_weak(const struct tag_info *tag1, const struct tag_info *tag2) {
  if (tag1->mask == tag2->mask) {
    if (tag1->id >= tag2->id) {
      return 1;
    } else {
      return 2;
    }
  } else if (tag1->mask > tag2->mask) {
      return 1;
  } else {
      return 2;
  }
}

#endif
