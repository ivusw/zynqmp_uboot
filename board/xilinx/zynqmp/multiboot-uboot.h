///////////////////////////////////////////////////////////////////////////////
//  _____                                  _
// |_   _|                                (_)
//   | |  _ __  _ __   _____   ___   _ ___ _  ___  _ __
//   | | | '_ \| '_ \ / _ \ \ / / | | / __| |/ _ \| '_  |
//  _| |_| | | | | | | (_) \ V /| |_| \__ \ | (_) | | | |
// |_____|_| |_|_| |_|\___/ \_/  \__,_|___/_|\___/|_| |_|
//
//
// Copyright (c) 2021  Innovusion, Inc.
// All Rights Reserved.
//
//    multiboot-uboot.h
//
///////////////////////////////////////////////////////////////////////////////

#ifndef LIDAR_APP_MULTIBOOT_UBOOT_H_
#define LIDAR_APP_MULTIBOOT_UBOOT_H_

#include "upgrade.h"

#include <common.h>

// load 2 Tags into memory, then find current tag
// set dts, linux, rootfs offset and size

#define tag_ddr_addr 0x4100000
#define tag_ddr_size 0x80000

void multiboot_uboot(void) {
  char cmd[256];
	char *buf;
  int curr_tag;
  char *curr_taginfo;
  struct tag_info tag;
  char val[32];

  struct tag_info tag1;
  struct tag_info tag2;

  run_command("sf probe 0 50000000 0", 0);

  // read tags
  snprintf(cmd, sizeof(cmd),
           "sf read 0x%08x 0x%08x 0x%08x",
           tag_ddr_addr, TAG1_CONFIRM_OFFSET, tag_ddr_size);
  run_command(cmd, 0);

  buf = (char *)map_physmem(tag_ddr_addr, tag_ddr_size, MAP_NOCACHE);

  curr_tag = get_current_tag(buf, buf + (TAG2_CONFIRM_OFFSET - TAG1_CONFIRM_OFFSET));
  printf("multiboot: current tag %d\n", curr_tag);
  switch (curr_tag) {
    case 1:
      curr_taginfo = buf + (TAG1_INFO_OFFSET - TAG1_CONFIRM_OFFSET);
      analyse_tag(curr_taginfo, kMaxTagInfoSize, &tag);
      break;

    case 2:
      curr_taginfo = buf + (TAG2_INFO_OFFSET - TAG1_CONFIRM_OFFSET);
      analyse_tag(curr_taginfo, kMaxTagInfoSize, &tag);
      break;

    default:
      printf("multiboot error: can not find current tag\n");
      curr_taginfo = buf + (TAG1_INFO_OFFSET - TAG1_CONFIRM_OFFSET);
      analyse_tag(curr_taginfo, kMaxTagInfoSize, &tag1);
      curr_taginfo = buf + (TAG2_INFO_OFFSET - TAG1_CONFIRM_OFFSET);
      analyse_tag(curr_taginfo, kMaxTagInfoSize, &tag2);
      curr_tag = get_current_tag_info_weak(&tag1, &tag2);
      printf("multiboot: weak current tag %d\n", curr_tag);
      if (curr_tag == 1) {
        memcpy(&tag, &tag1, sizeof(struct tag_info));
      } else {
        memcpy(&tag, &tag2, sizeof(struct tag_info));
      }
      break;
  }

  printf("multiboot: mask = 0x%08x\n", tag.mask);

  printf("multiboot: linux mtd=%d, offset=0x%08x, used_len=0x%08x\n",
         tag.pi[PartLinux].mtd_num,
         tag.pi[PartLinux].offset,
         tag.pi[PartLinux].used_len);
  sprintf(val, "0x%08x", tag.pi[PartLinux].offset);
  env_set("kernel_offset", val);
  sprintf(val, "0x%08x", tag.pi[PartLinux].used_len);
  env_set("kernel_size", val);

  printf("multiboot: rootfs mtd=%d, offset=0x%08x, used_len=0x%08x\n",
         tag.pi[PartRootfs].mtd_num,
         tag.pi[PartRootfs].offset,
         tag.pi[PartRootfs].used_len);
  sprintf(val, "0x%08x", tag.pi[PartRootfs].offset);
  env_set("rootfs_offset", val);
  sprintf(val, "0x%08x", tag.pi[PartRootfs].used_len);
  env_set("rootfs_size", val);

  printf("multiboot: dts mtd=%d, offset=0x%08x, used_len=0x%08x\n",
         tag.pi[PartDts].mtd_num,
         tag.pi[PartDts].offset,
         tag.pi[PartDts].used_len);
  sprintf(val, "0x%08x", tag.pi[PartDts].offset);
  env_set("fdt_offset", val);
  sprintf(val, "0x%08x", tag.pi[PartDts].used_len);
  env_set("fdt_size", val);

  unmap_physmem(buf, MAP_NOCACHE);
}

#endif
