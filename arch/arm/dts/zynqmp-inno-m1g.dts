/dts-v1/;

#include "zynqmp.dtsi"
#include "zynqmp-clk-ccf.dtsi"
#include "zynqmp-inno-pl.dtsi"
/*
#include <dt-bindings/input/input.h>
#include <dt-bindings/interrupt-controller/irq.h>
#include <dt-bindings/gpio/gpio.h>
#include <dt-bindings/pinctrl/pinctrl-zynqmp.h>
#include <dt-bindings/phy/phy.h>
*/

/ {
	model = "Innovusion ZynqMP 4EV";
	compatible = "xlnx,zynqmp-zcu100-revC", "xlnx,zynqmp-zcu100", "xlnx,zynqmp";

	aliases {
		ethernet0 = &gem1;
		rtc0 = &rtc;
		i2c0 = &i2c0;
		i2c1 = &i2c1;
		serial0 = &uart0;
		serial1 = &uart1;
		spi0 = &qspi;
		spi1 = &spi0;
	};

	chosen {
		/* bootargs = "earlycon clk_ignore_unused"; */
		bootargs = "root=/dev/ram0 ro rootwait earlyprintk clk_ignore_unused uio_pdrv_genirq.of_id=generic-uio debug";
		/* bootargs = "root=/dev/ram0 ro rootwait rootfstype=cramfs earlyprintk clk_ignore_unused uio_pdrv_genirq.of_id=generic-uio debug"; */
		/* bootargs = "root=/dev/mtdblock3 rw rootwait rootfstype=jffs2 earlyprintk clk_ignore_unused uio_pdrv_genirq.of_id=generic-uio debug"; */
		stdout-path = "serial1:115200n8";
	};

	memory@0 {
		device_type = "memory";
		reg = <0x0 0x0 0x0 0x3ff00000>;
	};

	reserved-memory {
	   #address-cells = <2>;
	   #size-cells = <2>;
	   ranges;

	   reserved: buffer@0x10000000 {
	      /*no-map;*/
	      reg = <0x0 0x10000000 0x0 0x10000000>;
	   };
	};

	reserved-driver@0 {
	   compatible = "xlnx,reserved-memory";
	   memory-region = <&reserved>;
	};

};

&gpio {
	emio-gpio-width = <32>;
	gpio-mask-high = <0x0>;
	gpio-mask-low = <0x5600>;
	status = "okay";
};

&gem1 {
	status = "okay";

	phy-mode = "rgmii";
	phy-handle = <&phy7>;
	phy7: phy@7 {
		reg = <7>;
	};
};

&fpd_dma_chan1 {
	status = "okay";
};

&fpd_dma_chan2 {
	status = "okay";
};

&fpd_dma_chan3 {
	status = "okay";
};

&fpd_dma_chan4 {
	status = "okay";
};

&fpd_dma_chan5 {
	status = "okay";
};

&fpd_dma_chan6 {
	status = "okay";
};

&fpd_dma_chan7 {
	status = "okay";
};

&fpd_dma_chan8 {
	status = "okay";
};

&i2c0 {
	status = "okay";
	clock-frequency = <400000>;
/*
	scl-gpios = <&gpio 66 GPIO_ACTIVE_HIGH>;
	sda-gpios = <&gpio 67 GPIO_ACTIVE_HIGH>;
*/
};

&i2c1 {
	status = "okay";
	clock-frequency = <400000>;
/*
	scl-gpios = <&gpio 8 GPIO_ACTIVE_HIGH>;
	sda-gpios = <&gpio 9 GPIO_ACTIVE_HIGH>;
*/
};

&spi0 {
	/*speed-hz = <50000000>;*/
	status = "okay";
	is-decoded-cs = <0>;
	num-cs = <1>;
	spidev@0x00 {
		#address-cells = <0x1>;
		#size-cells = <0x0>;
		compatible = "spidev";
         	status = "okay";
		spi-max-frequency = <1000000>;
		reg = <0>;
	};
};

&qspi {
	status = "okay";
	is-dual = <0>;
	num-cs = <1>;
	flash@0 {
		compatible = "m25p80", "jedec,spi-nor";
		#address-cells = <1>;
		#size-cells = <1>;
		reg = <0x0>;
		spi-rx-bus-width = <4>;
		spi-tx-bus-width = <4>;
		spi-max-frequency = <50000000>;
		partition@GOLDEN-fsbl {
			label = "GOLDEN-fsbl";
			reg = <0x0 0x00100000>;
		};
		partition@jffs2 {
			label = "jffs2";
			reg = <0x00100000 0x00480000>;
		};
		partition@Tag1Confirm {
			label = "Tag1Confirm";
			reg = <0x00580000 0x00020000>;
		};
		partition@Tag1Info {
			label = "Tag1Info";
			reg = <0x005A0000 0x00020000>;
		};
		partition@Tag2Confirm {
			label = "Tag2Confirm";
			reg = <0x005C0000 0x00020000>;
		};
		partition@Tag2Info {
			label = "Tag2Info";
			reg = <0x005E0000 0x00020000>;
		};
		partition@A-boot {
			label = "A-boot";
			reg = <0x00600000 0x00AE0000>;
		};
		partition@A-dts {
			label = "A-dts";
			reg = <0x010E0000 0x00020000>;
		};
		partition@A-linux {
			label = "A-linux";
			reg = <0x01100000 0x00600000>;
		};
		partition@A-rootfs {
			label = "A-rootfs";
			reg = <0x01B00000 0x01200000>;
		};
		partition@A-app-firmware {
			label = "A-app-firmware";
			reg = <0x02D00000 0x00600000>;
		};
		partition@A-app-pointcloud {
			label = "A-app-pointcloud";
			reg = <0x03300000 0x00A00000>;
		};
		partition@A-app-python {
			label = "A-app-python";
			reg = <0x03D00000 0x00600000>;
		};
		partition@B-boot {
			label = "B-boot";
			reg = <0x04300000 0x00AE0000>;
		};
		partition@B-dts {
			label = "B-dts";
			reg = <0x04DE0000 0x00020000>;
		};
		partition@B-linux {
			label = "B-linux";
			reg = <0x04E00000 0x00600000>;
		};
		partition@B-rootfs {
			label = "B-rootfs";
			reg = <0x05800000 0x01200000>;
		};
		partition@B-app-firmware {
			label = "B-app-firmware";
			reg = <0x06A00000 0x00600000>;
		};
		partition@B-app-pointcloud {
			label = "B-app-pointcloud";
			reg = <0x03700000 0x00A00000>;
		};
		partition@B-app-python {
			label = "B-app-python";
			reg = <0x07A00000 0x00600000>;
		};	
		partition@A-backup {
			label = "A-backup";
			reg = <0x01700000 0x00400000>;
		};
		partition@B-backup {
			label = "B-backup";
			reg = <0x05400000 0x00400000>;
		};
	};
};

&serdes {
	status = "okay";
};

&uart0 {
	status = "okay";
};

&uart1 {
	status = "okay";
	u-boot,dm-pre-reloc ;
};


&dwc3_0 {
	status = "okay";
	dr_mode = "host";
};

&lpd_watchdog {
	status = "okay";
};

&watchdog0 {
	timeout-sec = <15>;
	status = "okay";
};

&pss_ref_clk {
	clock-frequency = <33333333>;
};

&xilinx_ams {
	status = "okay";
};

&ams_ps {
	status = "okay";
};

&ams_pl {
	status = "okay";
};

&gic {
	num_cpus = <2>;
	num_interrupts = <96>;
};

&lpd_dma_chan1 {
	status = "okay";
};

&lpd_dma_chan2 {
	status = "okay";
};

&lpd_dma_chan3 {
	status = "okay";
};

&lpd_dma_chan4 {
	status = "okay";
};

&lpd_dma_chan5 {
	status = "okay";
};

&lpd_dma_chan6 {
	status = "okay";
};

&lpd_dma_chan7 {
	status = "okay";
};

&lpd_dma_chan8 {
	status = "okay";
};



